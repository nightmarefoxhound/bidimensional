package com.library.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.library.model.Book;
import com.library.repository.BookRepository;

@Service
public class BookService {
	
	@Autowired
	private BookRepository repo;
	
	public Book saveBook(Book book) {
		return repo.save(book);
	}

	public Book fetchBookByIsbn(String isbn) {
		return repo.findByIsbn(isbn);
	}
	
	public List<Book> buscarBook() {
		return repo.findAll();
	}
	
}
