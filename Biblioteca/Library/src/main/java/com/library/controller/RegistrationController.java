package com.library.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.library.model.User;
import com.library.service.RegistrationService;

@RestController
@RequestMapping("/api/v1")
public class RegistrationController {

	@Autowired
	private RegistrationService service;

	@PostMapping("/registerUser")
	@CrossOrigin(origins = "http://localhost:4200")
	public User registerUser(@RequestBody User user) throws Exception {

		String tempEmail = user.getEmail();
		if (tempEmail != null && !"".equals(tempEmail)) {
			User userObj = service.fetchUserByEmail(tempEmail);
			if (userObj != null) {
				throw new Exception("Usuario con email " + tempEmail + " ya se encuentra registrado.");
			}
		}

		User userObj = null;
		userObj = service.saveUser(user);
		return userObj;
	}
	
	@PostMapping("/loginUser")
	@CrossOrigin(origins = "http://localhost:4200")
	public User loginUser(@RequestBody User user) throws Exception {
		String tempEmail = user.getEmail();
		String tempPassword = user.getPassword();
		User userObj = null;
		
		System.out.println(tempEmail + tempPassword);
		
		if (tempEmail != null && tempPassword != null) {
			userObj = service.fetchUserByEmailAndPassword(tempEmail, tempPassword);
		}
		
		if (userObj == null) {
			throw new Exception("Datos incorrectos");
		}
		return userObj;
	}

	@GetMapping("/getUser")
	@CrossOrigin(origins = "http://localhost:4200")
	public List<User> listUser() {
		return service.buscarUser();
	}
}
