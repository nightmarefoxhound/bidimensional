package com.library.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.library.model.Book;
import com.library.service.BookService;

@RestController
@RequestMapping("/api/v1")
public class BookController {
	
	@Autowired
	private BookService service;
	
	@PostMapping("/registerBook")
	@CrossOrigin(origins = "http://localhost:4200")
	public Book registerBook(@RequestBody Book book) throws Exception {

		String tempISBN = book.getIsbn();
		if (tempISBN != null && !"".equals(tempISBN)) {
			Book bookObj = service.fetchBookByIsbn(tempISBN);
			if (bookObj != null) {
				throw new Exception("Libro con ISBN " + tempISBN + " ya se encuentra registrado.");
			}
		}

		Book bookObj = null;
		bookObj = service.saveBook(book);
		return bookObj;
	}
	
	@GetMapping("/getBook")
	@CrossOrigin(origins = "http://localhost:4200")
	public List<Book> listBook() {
		return service.buscarBook();
	}

}
