import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Usuario } from '../interfaces/usuario';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegistroService {

  private currentUserSubject: BehaviorSubject<Usuario>;
  public currentUser: Observable<Usuario>;

  constructor(
    private http: HttpClient
  ) {
    this.currentUserSubject = new BehaviorSubject<Usuario>(JSON.parse(localStorage.getItem('tipo')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): Usuario {
    return this.currentUserSubject.value;
  }

  public loginUserRemote(usuario: Usuario): Observable<any> {
    return this.http.post<any>('http://localhost:8080/api/v1/loginUser', usuario)
      .pipe(map(user => {
        if (user && user.tipo) {
          localStorage.setItem('tipo', user.tipo);
          localStorage.setItem('ROLE', user.nivel);
          this.currentUserSubject.next(user);
        }
        return user;
      }));
  }

  public logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('tipo');
    localStorage.removeItem('ROLE');
    this.currentUserSubject.next(null);
  }

  public altaClientes(usuario: Usuario): Observable<any> {
    return this.http.post<any>('http://localhost:8080/api/v1/registerUser', usuario);
  }

  public obtenerUsuarios(): Observable<any> {
    return this.http.get('http://localhost:8080/api/v1/getUser');
  }

}
