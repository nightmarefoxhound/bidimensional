import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Libro } from '../interfaces/libro';

@Injectable({
    providedIn: 'root'
})
export class LibrosService {

    constructor(
        private http: HttpClient
    ) { }

    public altaLibros(libro: Libro): Observable<any> {
        return this.http.post<any>('http://localhost:8080/api/v1/registerBook', libro);
    }

    public obtenerLibros(): Observable<any> {
        return this.http.get('http://localhost:8080/api/v1/getBook');
    }

}
