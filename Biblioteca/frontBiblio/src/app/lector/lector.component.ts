import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegistroService } from '../service/registro.service';

@Component({
  selector: 'app-lector',
  templateUrl: './lector.component.html',
  styleUrls: ['./lector.component.css']
})
export class LectorComponent implements OnInit {

  constructor(
    private service: RegistroService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  logout() {
    this.service.logout();
    this.router.navigate(['/']);
}

}
