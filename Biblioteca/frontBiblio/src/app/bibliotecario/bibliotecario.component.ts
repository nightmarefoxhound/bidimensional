import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegistroService } from '../service/registro.service';

@Component({
  selector: 'app-bibliotecario',
  templateUrl: './bibliotecario.component.html',
  styleUrls: ['./bibliotecario.component.css']
})
export class BibliotecarioComponent implements OnInit {

  constructor(
    private service: RegistroService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  logout() {
    this.service.logout();
    this.router.navigate(['/']);
}

}
