import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/interfaces/usuario';
import { RegistroService } from 'src/app/service/registro.service';

@Component({
  selector: 'app-alta-usuarios',
  templateUrl: './alta-usuarios.component.html',
  styleUrls: ['./alta-usuarios.component.css']
})
export class AltaUsuariosComponent implements OnInit {

  user = new Usuario();
  msg: string;

  tipoUsuario: any[];
  tipoUser: string;

  constructor(
    private service: RegistroService
  ) { }

  ngOnInit(): void {
    this.tipoUsuario = [
      { valor: 1, "tipo": "Administrador" },
      { valor: 2, "tipo": "Bibliotecario" },
      { valor: 3, "tipo": "Lector" }
    ]
  }

  public altaUsuario() {
    console.log(this.tipoUser);
    if (this.tipoUser === '1') {
      this.user.nivel = parseInt(this.tipoUser, 0);
      this.user.tipo = 'Administrador';
    } else if (this.tipoUser === '2') {
      this.user.nivel = parseInt(this.tipoUser, 0);
      this.user.tipo = 'Bibliotecario';
    } else {
      this.user.nivel = parseInt(this.tipoUser, 0);
      this.user.tipo = 'Lector';
    }

    this.service.altaClientes(this.user)
      .subscribe(
        data => {
          console.log(data);
        },
        error => {
          console.log(error);
          this.msg = 'Datos incorrectos, verificar correo y contraseña';
        }
      );
  }


}
