import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LibrosService } from 'src/app/service/libros.service';
import { RegistroService } from '../../service/registro.service';

@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: ['./administrador.component.css']
})
export class AdministradorComponent implements OnInit {

  showModuleLibros: boolean;
  showModuleUsuarios: boolean;
  showListaUsuarios: boolean;
  usuarios: any[];
  libros: any[];

  p: number;
  q: number;

  constructor(
    private service: RegistroService,
    private router: Router,
    private bookSer: LibrosService
  ) { }

  ngOnInit(): void {
    this.showModuleLibros = false;
    this.showModuleUsuarios = false;
    this.showListaUsuarios = true;

    this.obtenerUsuarios();
    this.obtenerLibros();
  }

  mostrarLista() {
    this.showModuleLibros = false;
    this.showModuleUsuarios = false;
    this.showListaUsuarios = true;
  }

  mostrarLibros() {
    this.showModuleLibros = true;
    this.showModuleUsuarios = false;
    this.showListaUsuarios = false;
  }

  mostrarUsuarios() {
    this.showModuleLibros = false;
    this.showModuleUsuarios = true;
    this.showListaUsuarios = false;
  }

  logout() {
    this.service.logout();
    this.router.navigate(['/']);
  }

  obtenerUsuarios() {
    this.service.obtenerUsuarios().subscribe(data => {
      this.usuarios = data;
      console.log(data);
    })
  }

  obtenerLibros() {
    this.bookSer.obtenerLibros().subscribe(data => {
      this.libros = data;
      console.log(data);
    })
  }

}
