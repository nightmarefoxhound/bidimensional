import { Component, OnInit } from '@angular/core';
import { Libro } from 'src/app/interfaces/libro';
import { LibrosService } from 'src/app/service/libros.service';

@Component({
  selector: 'app-alta-libros',
  templateUrl: './alta-libros.component.html',
  styleUrls: ['./alta-libros.component.css']
})
export class AltaLibrosComponent implements OnInit {

  book = new Libro();
  msg: string;

  constructor(
    private service: LibrosService
  ) { }

  ngOnInit(): void {
  }

  public altaLibro() {
    this.service.altaLibros(this.book)
      .subscribe(
        data => {
          console.log(data);
        },
        error => {
          console.log(error);
          this.msg = 'Algo sucedio en el guardado!!';
        }
      );
  }

}
