import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { RegistroService } from '../service/registro.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private service: RegistroService,
    private router: Router
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const currentUser = this.service.currentUserValue;

    if (currentUser) {
      if (next.data.roles && next.data.roles.indexOf(currentUser.tipo) === -1) {
        this.router.navigate(['/administrador']);
        return false;
      }
    }

    return true;
  }
}
