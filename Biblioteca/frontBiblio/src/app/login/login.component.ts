import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { RegistroService } from '../service/registro.service';
import { Usuario } from '../interfaces/usuario';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  /*user: Usuario[];
  usuario: Usuario;*/

  user = new Usuario();

  usuario: string;
  contra: string;

  nivel: number;
  tipo: string;
  username: string;

  msg: string;
  show: boolean;

  constructor(
    private loginService: RegistroService,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.show = false;
  }

  loginUser() {
    this.loginService.loginUserRemote(this.user)
      .subscribe(
        data => {
          console.log(data);
          if (data.nivel === 1){
            this.route.navigate(['/administrador'])
          } else if (data.nivel === 2) {
            this.route.navigate(['/bibliotecario'])
          } else {
            this.route.navigate(['/lector'])
          }
        },
        error => {
          console.log(error);
          this.show = true;
          this.msg = 'Datos incorrectos, verificar correo y contraseña';
        }
      );     
  }

}
 