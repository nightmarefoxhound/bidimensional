import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministradorComponent } from './administracion/administrador/administrador.component';
import { BibliotecarioComponent } from './bibliotecario/bibliotecario.component';
import { AuthGuard } from './guards/auth.guard';
import { LectorComponent } from './lector/lector.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'administrador', component: AdministradorComponent, canActivate: [AuthGuard], data: {role: '1'} },
  { path: 'bibliotecario', component: BibliotecarioComponent, canActivate: [AuthGuard], data: {role: '2'}},
  { path: 'lector', component: LectorComponent, canActivate: [AuthGuard], data: {role: '3'} }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
