import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AdministradorComponent } from './administracion/administrador/administrador.component';
import { BibliotecarioComponent } from './bibliotecario/bibliotecario.component';
import { LectorComponent } from './lector/lector.component';
import { AuthGuard } from './guards/auth.guard';
import { AltaUsuariosComponent } from './administracion/alta-usuarios/alta-usuarios.component';
import { AltaLibrosComponent } from './administracion/alta-libros/alta-libros.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistroComponent,
    AdministradorComponent,
    BibliotecarioComponent,
    LectorComponent,
    AltaUsuariosComponent,
    AltaLibrosComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxPaginationModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
