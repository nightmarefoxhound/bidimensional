import java.util.Scanner;

/**
 * Clase para ejecutar el algoritmo para generar una matriz caracol cuadrada
 * @author Daniel Alvarez
 */
public class MatrizM {

   public static void main(String[] args) {
      Scanner in = new Scanner(System.in);
      System.out.print("Dimensión del Arreglo: ");
      int n = in.nextInt();
      System.out.println();
      System.out.println(n + "x" + n);
      System.out.println();

      recorreArreglo(n);
   }

   public static int recorreArreglo(int n) {
      String[][] arr = new String[n][n];
      int a = 0;
      int b = n - 1;
      int valor = 1;

      for (int j = 0; j < arr.length; j++) {
         //Llena la primer fila
         for (int i = a; i <= b; i++) {
            arr[a][i] = valor++ + "\t";
         }

         //Llena la columna derecha
         for (int i = a+1; i <= b; i++) {
            arr[i][b] = valor++ + "\t";
         }

         //Llena la fila de abajo de derecha a izquierda
         for (int i = b-1; i >= a; i--) {
            arr[b][i] = valor++ + "\t";
         }

         //Llena la columna izquierda de abajo hacia arriba
         for (int i = b-1; i >= a+1; i--) {
            arr[i][a] = valor++ + "\t";
         }

         a++;
         b--;
      }

      for (int f = 0; f < arr.length; f++) {
         for (int i = 0; i < arr[f].length; i++) {
            System.out.print(arr[f][i]);
         }
         System.out.println();
      }
      return n;
   }
}